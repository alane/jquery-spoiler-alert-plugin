!function($) {

	var defaults = {
		obscure: "jumble",
		color: "#E8E8E8",
		revealOnHover: true
	};

	$.fn.spoilerAlert = function(options) {
		var settings = $.extend({}, defaults, options); 

		//in the DOM iterate through each instance of (in this case, .spoilerAlert)
		return this.each(function() {

			//save the specific instance of this in a variable
			var el = $(this);
			var pText = $(this).text(); 
			var originalText = $(this).text();
			//get the background-color from the css and store it
			var originalBackgroundColor = $(this).css("background-color");
			var originalTextColor = $(this).css("color");

			//obscure options
			 var spoiler = function() {
				switch (settings.obscure) {

					//jumbles text
					case 'jumble':

						//create ending array for later use
						var jumbledLetters = [];

						//split var pText every character;
						var elSplit = pText.split("");

						//iterate through array
						for (var i = 0; i < elSplit.length; i++) {
							var singLetter = elSplit[i];
							jumbledLetters.push(singLetter);
							//console.log(singLetter);

							//splice out and return character at index 2
							var splicedLetters = elSplit.splice(2, 1);

							//put chars in the ending array
							jumbledLetters.push(splicedLetters); 
						}
						var jumbledText = el.text(jumbledLetters.join(''));
			

					break;    
					
					//reverses text
					case 'reverse':
						var reverse = pText.split(" ").reverse().join(" ");
						el.text(reverse);

					break;

					
					
					//displays text as pig latin
					case 'pigLatin':
						
						//find p tags; separate words on the spaces and put into an array called wordSet
						var wordSet = pText.split(" "); 

						//create final array for later use
						var pigLatinArray = [];

						//create var for later pushing of pig latin words
						var tempPig = '';
						

						//iterate through the array of words
						for (var i = 0; i < wordSet.length; i++) {

							//get each word separately
							var singWord = wordSet[i];
						
							//identify first letter
							var firstLetter = singWord.charAt(0);

							//identify second letter
							var secondLetter = singWord.charAt(1);

							if ("aeiou".indexOf(firstLetter) > 0) {

								//leave first letter there, and add "ay" to end of word
								var pigWord = singWord.concat("ay");

								//push finished vowel-starting pig latin word to array
								tempPig = pigWord;

							//if the word starts with a "c", check if the next letter is "h"
							} else if (firstLetter === 'c' || 't' && secondLetter === 'h') {

								//move both those letters to the end, then add the "ay"
								var pigWord2 = singWord.slice(2).concat(firstLetter).concat(secondLetter).concat("ay")
								
								//push th and ch-starting pig latin words to array
								tempPig = pigWord2;
						 	
						 	//if the first letter is empty (if there's an extra space at the end of the paragraph, the first letter will be empty)
						 	} else if (firstLetter === '') {
						 		
						 		//set tepPig to nothing (deletes the space)
						 		tempPig = '';

						 	} else {

								//take letter out; move it to the end of the word; add "ay" to the very end of word
								var pigWord3 = singWord.slice(1).concat(firstLetter).concat("ay");
								
								//push the other pig latin words to the array
								tempPig = pigWord3;
							} 

							//this doesn't handle all punctuation
							//if temPig contains a period
							if (tempPig.indexOf('.') > 0 ) {

								//move period to the end of the word
								tempPig = tempPig.split('.').join('').concat('.');
							}

							//if tempPig contains a comma
							if (tempPig.indexOf(',') > 0 ) {
								//move period to the end of the word
								tempPig = tempPig.split(',').join('').concat(',');
							}

							//push the pig latin word to the final array
							pigLatinArray.push(tempPig);
						}
						
						var pigLatinP = el.text(pigLatinArray.join(' '));  


					break;
				
					//hides text and displays show button 
					case 'warning':
						el.hide().after("<button>Show Spoiler</button>"); 
						
						el.next().click(function() {
							el.show();
							$(this).remove(); 
						})
					break;

					
					
					//Displays text as a bar in the color of your choosing
					case 'blackOut': 
						el.css({backgroundColor: settings.color}); 

					break;

				} 
			}; 
spoiler();
			//pass in the users choice of color
			el.css({color: settings.color});

			//if revealOnHover is true
			if (settings.revealOnHover) {
				//cache modified values
				var modifyText = el.text(); 
				var modifyBackgroundColor = $(this).css("background-color");
				var modifyTextColor = $(this).css("color");


				//on hover
				$(this).hover(function() { 

					//replace el.text with original text
					$(this).text(originalText); 
					$(this).css({backgroundColor: originalBackgroundColor});
					$(this).css({color: originalTextColor});

				//change back when not in hover state
				}, function() {
					$(this).text(modifyText);
					$(this).css({backgroundColor: modifyBackgroundColor});
					$(this).css({color: modifyTextColor});
				})
			}

		});
	};	
}(window.jQuery);
